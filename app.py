from flask import Flask
from flask import request

import sys
import serial
import time
import cobs
import binascii

from array import array


app = Flask(__name__)


ser = serial.Serial('/dev/ttyUSB0', baudrate=38400, bytesize=8, parity='N', stopbits=1)


def print_hex(h):
    print("".join("{:02x} ".format(x) for x in h))


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/stream', methods=['GET', 'POST'])
def stream():
    if request.method == 'GET':
        command = b"\xFF\x14\x00\x06\x00\x85\x86\x00\x00\x00\x68\x65\x6C\x6C\x6F\x77\x6F\x72\x6C\x64\x21"
        enc = cobs.encode(command)
        enc += b'0x00'
        # ser.write(enc)
        return "sent get"

    if request.method == 'POST':
        a = request.form.to_dict()
        # print(type(a))
        b = next(iter(a))
        b = b.rstrip()
        print("Incoming msg: " + b)
        command = bytes.fromhex(b)
        # print_hex(command)

        enc = cobs.encode(command)
        enc = list(enc)
        enc.append(0x00)
        enc = bytes(enc)
        # enc += b'0x00'

        # print_hex(enc)

        ser.write(enc)
        return "sent post!"


if __name__ == '__main__':
    app.run()
